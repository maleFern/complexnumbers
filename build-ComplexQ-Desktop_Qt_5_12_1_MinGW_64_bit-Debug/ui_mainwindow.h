/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *labelBRe;
    QSpinBox *spinBoxARe;
    QSpinBox *spinBoxBIm;
    QLabel *labelInfoRes;
    QLabel *labelARe;
    QLabel *labelA;
    QLabel *labelResult;
    QSpinBox *spinBoxBRe;
    QPushButton *pushButtonPerform;
    QSpinBox *spinBoxAIm;
    QLabel *labelBIm;
    QComboBox *comboBox;
    QLabel *labelAIm;
    QLabel *labelB;
    QLabel *labelN;
    QSpinBox *spinBoxN;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(470, 413);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        labelBRe = new QLabel(centralWidget);
        labelBRe->setObjectName(QString::fromUtf8("labelBRe"));
        labelBRe->setGeometry(QRect(260, 170, 81, 20));
        spinBoxARe = new QSpinBox(centralWidget);
        spinBoxARe->setObjectName(QString::fromUtf8("spinBoxARe"));
        spinBoxARe->setGeometry(QRect(350, 80, 42, 22));
        spinBoxARe->setMinimum(-99);
        spinBoxBIm = new QSpinBox(centralWidget);
        spinBoxBIm->setObjectName(QString::fromUtf8("spinBoxBIm"));
        spinBoxBIm->setEnabled(true);
        spinBoxBIm->setGeometry(QRect(350, 202, 42, 20));
        spinBoxBIm->setMinimum(-99);
        labelInfoRes = new QLabel(centralWidget);
        labelInfoRes->setObjectName(QString::fromUtf8("labelInfoRes"));
        labelInfoRes->setGeometry(QRect(80, 30, 64, 16));
        labelARe = new QLabel(centralWidget);
        labelARe->setObjectName(QString::fromUtf8("labelARe"));
        labelARe->setGeometry(QRect(260, 80, 81, 20));
        labelA = new QLabel(centralWidget);
        labelA->setObjectName(QString::fromUtf8("labelA"));
        labelA->setGeometry(QRect(80, 90, 171, 31));
        labelResult = new QLabel(centralWidget);
        labelResult->setObjectName(QString::fromUtf8("labelResult"));
        labelResult->setGeometry(QRect(150, 30, 261, 16));
        spinBoxBRe = new QSpinBox(centralWidget);
        spinBoxBRe->setObjectName(QString::fromUtf8("spinBoxBRe"));
        spinBoxBRe->setGeometry(QRect(350, 170, 42, 22));
        spinBoxBRe->setMinimum(-99);
        pushButtonPerform = new QPushButton(centralWidget);
        pushButtonPerform->setObjectName(QString::fromUtf8("pushButtonPerform"));
        pushButtonPerform->setGeometry(QRect(170, 290, 93, 28));
        spinBoxAIm = new QSpinBox(centralWidget);
        spinBoxAIm->setObjectName(QString::fromUtf8("spinBoxAIm"));
        spinBoxAIm->setGeometry(QRect(350, 110, 42, 22));
        spinBoxAIm->setMinimum(-99);
        labelBIm = new QLabel(centralWidget);
        labelBIm->setObjectName(QString::fromUtf8("labelBIm"));
        labelBIm->setGeometry(QRect(260, 200, 83, 16));
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setEnabled(true);
        comboBox->setGeometry(QRect(80, 50, 301, 22));
        labelAIm = new QLabel(centralWidget);
        labelAIm->setObjectName(QString::fromUtf8("labelAIm"));
        labelAIm->setGeometry(QRect(260, 110, 91, 20));
        labelB = new QLabel(centralWidget);
        labelB->setObjectName(QString::fromUtf8("labelB"));
        labelB->setGeometry(QRect(80, 170, 211, 41));
        labelN = new QLabel(centralWidget);
        labelN->setObjectName(QString::fromUtf8("labelN"));
        labelN->setGeometry(QRect(270, 230, 31, 21));
        spinBoxN = new QSpinBox(centralWidget);
        spinBoxN->setObjectName(QString::fromUtf8("spinBoxN"));
        spinBoxN->setGeometry(QRect(290, 230, 42, 22));
        spinBoxN->setMinimum(1);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 470, 26));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        labelBRe->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\320\246\320\265\320\273\320\260\321\217 \321\207\320\260\321\201\321\202\321\214</p></body></html>", nullptr));
        labelInfoRes->setText(QApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202:", nullptr));
        labelARe->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\320\246\320\265\320\273\320\260\321\217 \321\207\320\260\321\201\321\202\321\214</p></body></html>", nullptr));
        labelA->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">\320\232\320\276\320\274\320\277\320\273\320\265\320\272\321\201\320\275\320\276\320\265 \321\207\320\270\321\201\320\273\320\276 a:</span></p></body></html>", nullptr));
        labelResult->setText(QString());
        pushButtonPerform->setText(QApplication::translate("MainWindow", "\320\222\320\253\320\247\320\230\320\241\320\233\320\230\320\242\320\254", nullptr));
        labelBIm->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\320\234\320\275\320\270\320\274\320\260\321\217 \321\207\320\260\321\201\321\202\321\214</p></body></html>", nullptr));
        labelAIm->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\320\234\320\275\320\270\320\274\320\260\321\217 \321\207\320\260\321\201\321\202\321\214</p></body></html>", nullptr));
        labelB->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">\320\232\320\276\320\274\320\277\320\273\320\265\320\272\321\201\320\275\320\276\320\265 \321\207\320\270\321\201\320\273\320\276 b:</span></p></body></html>", nullptr));
        labelN->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">n:</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
