#ifndef TEST_H
#define TEST_H
#include <QtCore>
#include <QtTest/QtTest>


class Test_Complex : public QObject
{
    Q_OBJECT

public:
    Test_Complex();

private slots:
    void multi();
    void addition();
    void subtraction();
    void equal();
    void powQ();
    void sqrtQ();

//   void test_case3();
//    void test_case4();
//    void test_case5();
//    void test_case6();

};

#endif // TEST_H
