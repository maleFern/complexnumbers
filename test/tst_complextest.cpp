#include <QtTest>
#include "test.h"
#include <../Complex/complexnumber.h>
// add necessary includes here
bool compareVec(const QVector<float> &vec1, const QVector<float> &vec2) {
    if (vec1 == vec2)
        return true;
    return false;
}

Test_Complex::Test_Complex()
{

}

void Test_Complex::multi()
{
    Complex var1(1,2);
    Complex var2(4,8);
    Complex var3(0,0);
    var3.setIm((var1*var2).imGet());
    var3.setRe((var1*var2).reGet());
    Complex var4(-12,16);
    QCOMPARE(var3.imGet(), var4.imGet());
    QCOMPARE(var3.reGet(), var4.reGet());
}
void Test_Complex::addition()
{
    Complex var1(4,2);
    Complex var2(-1,2);
    Complex var3(0,0);
    var3.setIm((var1+var2).imGet());
    var3.setRe((var1+var2).reGet());
    Complex var4(3,4);
    QCOMPARE(var3.imGet(), var4.imGet());
    QCOMPARE(var3.reGet(), var4.reGet());

}
void Test_Complex::subtraction()
{
    Complex var1(9,1);
    Complex var2(-8,6);
    Complex var3(0,0);
    var3.setIm((var1-var2).imGet());
    var3.setRe((var1-var2).reGet());
    Complex var4(17,-5);
    QCOMPARE(var3.imGet(), var4.imGet());
    QCOMPARE(var3.reGet(), var4.reGet());
}
void Test_Complex::equal()
{
    Complex var1(9,1);
    Complex var2(9,1);
    QCOMPARE(true, var1 == var2);
    Complex var3(-9,1);
    Complex var4(9,1);
    QCOMPARE(false, var3 == var4);

}
void Test_Complex::powQ()
{
    Complex var1(7,1);

    Complex var2(2,-6);

    Complex var3(0,0);
    var1.powQ(6);
    var3.setIm(var1.imGet());
    var3.setRe(var1.reGet());
    Complex var4(82368,94024);
    QCOMPARE(var1.imGet(), var4.imGet());
    QCOMPARE(var1.reGet(), var4.reGet());
    var2.powQ(3);
    var3.setIm(var2.imGet());
    var3.setRe(var2.reGet());
    Complex var5(-208,144);
    QCOMPARE(var2.imGet(), var5.imGet());
    QCOMPARE(var2.reGet(), var5.reGet());
}

void Test_Complex::sqrtQ()
{
    Complex var1(4,0);
    Complex var3(0,0);
    var1.sqrtQ(2);
    var3.setIm(var1.imGet());
    var3.setRe(var1.reGet());
    Complex var2(1,0);
    QCOMPARE(var2.imGet(), var3.imGet());
    QCOMPARE(var2.reGet(), var3.reGet());
}

