#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "complexnumb.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->comboBox->addItem("+");
    ui->comboBox->addItem("-");
    ui->comboBox->addItem("*");
    ui->comboBox->addItem("==");
    ui->comboBox->addItem("^");
    ui->comboBox->addItem("sqrt");
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButtonPerform_clicked()
{
    Complex var1(0,0);
    Complex var2(0,0);
    var1.setRe(ui->spinBoxARe->value());
    var1.setIm(ui->spinBoxAIm->value());

    Complex var3(0,0);
    if("+" == ui->comboBox->currentText())
    {
        var2.setRe(ui->spinBoxBRe->value());
        var2.setIm(ui->spinBoxBIm->value());

        var3 = var1 + var2;
        ui->labelResult->setText(QString::number(var3.reGet()) + " " + QString::number(var3.imGet()) + "i");
    }
    else if("-" == ui->comboBox->currentText())
    {
        var2.setRe(ui->spinBoxBRe->value());
        var2.setIm(ui->spinBoxBIm->value());
        var3 = var1 - var2;
        ui->labelResult->setText(QString::number(var3.reGet()) + " " + QString::number(var3.imGet()) + "i");
    }
    else if("*" == ui->comboBox->currentText())
    {
        var2.setRe(ui->spinBoxBRe->value());
        var2.setIm(ui->spinBoxBIm->value());
        var3 = var1 * var2;
        ui->labelResult->setText(QString::number(var3.reGet()) + " " + QString::number(var3.imGet()) + "i");
    }
    else if("==" == ui->comboBox->currentText())
    {
        var2.setRe(ui->spinBoxBRe->value());
        var2.setIm(ui->spinBoxBIm->value());
        bool flag = var1 == var2;
        if(flag)
        { ui->labelResult->setText("Yes");}
        else
            ui->labelResult->setText("No");
    }
    else if("^" == ui->comboBox->currentText())
    {
        int n = ui->spinBoxN->value();
        var1.powQ(n);
        ui->labelResult->setText(QString::number(var1.reGet()) + " " + QString::number(var1.imGet()) + "i");
    }
    else if("sqrt" == ui->comboBox->currentText())
    {
        int n = ui->spinBoxN->value();
        var1.sqrtQ(n);
        ui->labelResult->setText(QString::number(var1.reGet()) + " " + QString::number(var1.imGet()) + "i");
    }
}


