#ifndef COMPLEXNUMBER_H
#define COMPLEXNUMBER_H
#include "math.h"
class Complex {
    int re;
    int im;
public:
    Complex(int _re, int _im)
    {
        re = _re;
        im = _im;
    }


    Complex operator*(Complex var2)
    {
        int _re = var2.re * re - var2.im * im;
        var2.im = var2.re * im + var2.im * re;
        var2.re = _re;
        return var2;
    }

    Complex operator+(Complex var2)
    {
        int _re = re + var2.re;
        var2.im = im + var2.im;
        var2.re = _re;
        return var2;
    }
    Complex operator-(Complex var2)
    {
        int _re = re - var2.re;
        var2.im = im - var2.im;
        var2.re = _re;
        return var2;
    }
    bool operator==(Complex var2)
    {
        if(re == var2.re && im == var2.im)
            return true;
        return false;
    }


    int reGet()
    {
        return re;
    }
    void setRe(int _re)
    {
        re = _re;
    }
    int imGet()
    {
        return im;
    }
    void setIm(int _im)
    {
        im = _im;
    }
    void powQ(int n)
    {
        Complex before(re,im);
        for(int k = 1; k < n; k++)
            *this = *this * before;
    }
    void sqrtQ(int n)
    {
        float z = sqrt(re * re + im * im);
        float f = acos(re/z);
        re = pow(z, 1/n) * cosf(f/n);
        im = pow(z, 1/n) * sinf(f/n);

    }
};
#endif // COMPLEXNUMBER_H
